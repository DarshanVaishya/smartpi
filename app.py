from flask import *
import pymysql
import smtplib, random, string
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


app = Flask(__name__)
app.secret_key = "smartpi_secretkey"


@app.route('/')
def adminLoadLogin():
    session['Username'] = ""
    session['Role'] = ""
    return render_template('Login.html')


@app.route('/authenticate', methods=['POST'])
def authenticate():
    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()

    loginUsername = request.form['loginUsername']
    loginPassword = request.form['loginPassword']
    cur.execute(f"SELECT LoginUsername, LoginPwd, LoginRole FROM loginmaster WHERE LoginUsername='{loginUsername}'")
    data = cur.fetchone()

    cur.close()
    connection.close()

    if data is None:
        return render_template('Login.html', message="Invalid Username")
    if loginUsername == data[0] and loginPassword != data[1]:
        return render_template('Login.html', message="Invalid Password")
    if loginUsername == data[0] and loginPassword == data[1]:
        session['Username'] = data[0]
        if data[2] == 'User':
            session['Role'] = 'User'
            return redirect('/user/loadDashboard')
        elif data[2] == 'Admin':
            session['Role'] = 'Admin'
            return redirect('/admin/loadDashboard')


@app.route('/admin/loadDashboard')
def adminLoadDashboard():
    if session['Role'] == 'Admin':
        return render_template('index.html')
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/viewAdmin')
def adminViewAdmin():
    if session['Role'] == 'Admin':
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()

        cur.execute("SELECT * FROM loginmaster WHERE LoginRole='Admin'")
        data = cur.fetchall()
        cur.close()
        connection.close()
        return render_template('viewAdmin.html', data=data)
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/addAdmin')
def add_admin():
    if session['Role'] == 'Admin':
        return render_template('addAdmin.html')
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/addAdminData', methods=['POST'])
def addAdminData():
    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()

    username = request.form['addAdminUsername']
    password = request.form['addAdminPassword']
    cur.execute(f"INSERT INTO loginmaster(LoginUsername, LoginPwd, LoginRole, LoginStatus) values('{username}', '{password}', 'Admin', 'Active');")
    connection.commit()
    cur.close()
    connection.close()
    return redirect('/admin/viewAdmin')


@app.route('/admin/editAdmin', methods=['GET'])
def editAdmin():
    if session['Role'] == 'Admin':
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()

        adminId = request.args.get('adminId')
        cur.execute(f'select * from loginmaster where LoginId={adminId}')
        data = cur.fetchone()
        cur.close()
        connection.close()
        return render_template('editAdmin.html', data=data)
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/editAdminData', methods=['GET', 'POST'])
def editAdminData():
    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()

    adminId = request.args.get('adminId')
    username = request.form['editAdminUsername']
    password = request.form['editAdminPassword']

    cur.execute(f"update loginmaster set LoginUsername='{username}', LoginPwd='{password}' where Loginid={adminId}")
    connection.commit()
    cur.close()
    connection.close()
    return redirect('/admin/viewAdmin')


@app.route('/admin/deleteAdmin', methods=['GET'])
def adminDeleteAdmin():
    if session['Role'] == 'Admin':
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()

        adminId = request.args.get('adminId')
        cur.execute(f"delete from loginmaster where LoginId={adminId}")
        connection.commit()
        cur.close()
        connection.close()
        return redirect('/admin/viewAdmin')
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/viewTopics')
def adminViewTopics():
    if session['Role'] == 'Admin':
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()

        cur.execute("select * from topicmaster")
        data = cur.fetchall()

        cur.close()
        connection.close()
        return render_template('viewTopic.html', data=data)
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/addTopic')
def adminAddTopic():
    if session['Role'] == 'Admin':
        return render_template('addTopic.html')
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/addTopicData', methods=['POST'])
def adminAddTopicData():
    topicName = request.form['topicName']
    topicDescription = request.form['topicDescription']

    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()
    cur.execute(f"insert into topicmaster(TopicName, TopicDescription) values('{topicName}', '{topicDescription}')")

    connection.commit()
    cur.close()
    connection.close()
    return redirect('/admin/viewTopics')


@app.route('/admin/editTopic', methods=['GET'])
def adminEditTopic():
    if session['Role'] == 'Admin':
        topicId = request.args.get('topicId')
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()
        cur.execute(f"select * from topicmaster where TopicId={topicId}")
        data = cur.fetchone()
        cur.close()
        connection.close()
        return render_template('editTopic.html', data=data)
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/editTopicData', methods=['GET', 'POST'])
def adminEditTopicData():
    topicId = request.args.get('topicId')
    topicName = request.form['topicName']
    topicDescription = request.form['topicDescription']

    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()
    cur.execute(f"update topicmaster set TopicName='{topicName}', TopicDescription='{topicDescription}' where TopicId={topicId}")
    connection.commit()
    cur.close()
    connection.close()
    return redirect('/admin/viewTopics')


@app.route('/admin/deleteTopic', methods=['GET'])
def adminDeleteTopic():
    if session['Role'] == 'Admin':
        topicId = request.args.get('topicId')
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()

        cur.execute(f"delete from topicmaster where TopicId='{topicId}'")
        connection.commit()
        cur.close()
        connection.close()
        return redirect('/admin/viewTopics')
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/viewQuestions')
def adminViewQuestions():
    if session['Role'] == 'Admin':
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()
        cur.execute(f"select * from questionsmaster")
        data = cur.fetchall()
        return render_template('viewQuestions.html', data=data)
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')

@app.route('/admin/addQuestions')
def adminAddQuestions():
    if session['Role'] == 'Admin':
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()
        cur.execute(f"select TopicName from topicmaster")
        data = cur.fetchall()
        cur.close()
        connection.close()
        return render_template('addQuestion.html', data=data)
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/addQuestionData', methods=['POST'])
def adminAddQuestionData():
    topicName = request.form.get('topicName')
    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()
    cur.execute(f"select TopicId from topicmaster where TopicName='{topicName}'")
    topicId = cur.fetchone()[0]
    Q1 = request.form['Q1']
    Q1_K = request.form['Q1_K']
    Q2 = request.form['Q2']
    Q2_K = request.form['Q2_K']
    Q3 = request.form['Q3']
    Q3_K = request.form['Q3_K']
    Q4 = request.form['Q4']
    Q4_K = request.form['Q4_K']
    Q5 = request.form['Q5']
    Q5_K = request.form['Q5_K']
    cur.execute(f"insert into questionsmaster values({topicId}, '{topicName}', '{Q1}', '{Q1_K}', '{Q2}', '{Q2_K}', '{Q3}', '{Q3_K}', '{Q4}', '{Q4_K}', '{Q5}', '{Q5_K}')")
    connection.commit()
    cur.close()
    connection.close()
    return redirect('/admin/viewQuestions')


@app.route('/admin/editQuestion')
def adminEditQuestion():
    if session['Role'] == 'Admin':
        topicId = request.args.get('topicId')
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()
        cur.execute(f"select * from questionsmaster where topicId={topicId}")
        data = cur.fetchone()
        cur.close()
        connection.close()
        return render_template('editQuestion.html', data=data)
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/editQuestionData', methods=["GET", "POST"])
def adminEditQuestionData():
    topicId = request.args.get('topicId')
    Q1 = request.form['Q1']
    Q1_K = request.form['Q1_K']
    Q2 = request.form['Q2']
    Q2_K = request.form['Q2_K']
    Q3 = request.form['Q3']
    Q3_K = request.form['Q3_K']
    Q4 = request.form['Q4']
    Q4_K = request.form['Q4_K']
    Q5 = request.form['Q5']
    Q5_K = request.form['Q5_K']

    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()
    cur.execute(f"update questionsmaster set Q1='{Q1}', Q1_K='{Q1_K}', Q2='{Q2}', Q2_K='{Q2_K}', Q3='{Q3}', Q3_K='{Q3_K}', Q4='{Q4}', Q4_K='{Q4_K}', Q5='{Q5}', Q1_K='{Q5_K}' where TopicId={topicId}")
    connection.commit()
    cur.close()
    connection.close()
    return redirect('/admin/viewQuestions')


@app.route('/admin/deleteQuestion')
def adminDeleteQuestion():
    if session['Role'] == 'Admin':
        topicId = request.args.get('topicId')
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()
        cur.execute(f"delete from questionsmaster where TopicId={topicId}")
        connection.commit()
        cur.close()
        connection.close()
        return redirect('/admin/viewQuestions')
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/manageUser')
def adminManagerUser():
    if session['Role'] == 'Admin':
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()
        cur.execute(f"select * from registermaster")
        data = cur.fetchall()
        cur.close()
        connection.close()
        return render_template('manageUser.html', data=data)
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/admin/removeUser', methods=['GET'])
def adminRemoveUser():
    if session['Role'] == 'Admin':
        username = request.args.get('username')
        connection = pymysql.connect(
            host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
            user='root',
            password='rootroot',
            db='smartpi'
        )
        cur = connection.cursor()
        cur.execute(f"delete from registermaster where User_Name='{username}'")
        connection.commit()
        cur.execute(f"delete from loginmaster where LoginUsername='{username}'")
        connection.commit()
        cur.close()
        connection.close()
        return redirect('/admin/manageUser')
    elif session['Role'] == 'User':
        return redirect('/user/loadDashboard')
    else:
        return redirect('/')


@app.route('/register')
def register():
    return render_template('register.html')


@app.route('/registerData', methods=['POST'])
def registerData():
    FN = request.form['registerFN']
    LN = request.form['registerLN']
    email = request.form['registerEmail']
    username = email.split('@')[0]
    password = request.form['registerPassword']
    gender = request.form['gender']

    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()
    cur.execute(f"insert into registermaster(First_Name, Last_Name, Email, User_Name, User_Pwd, Gender) values('{FN}', '{LN}', '{email}', '{username}', '{password}', '{gender}')")
    cur.execute(f"insert into loginmaster(LoginUsername, LoginPwd, LoginRole, LoginStatus) values('{username}', '{password}', 'User', 'Active')")
    connection.commit()
    cur.close()
    connection.close()
    return redirect('/')


@app.route('/user/loadDashboard')
def userLoadDashboard():
    if session['Role'] == 'User':
        return render_template('userindex.html')
    elif session['Role'] == 'Admin':
        return redirect('/admin/loadDashboard')
    else:
        return redirect('/')

@app.route('/user/viewQuestion')
def userQuestions():
    if session['Role'] == 'User':
        return render_template('userQuestions.html')
    elif session['Role'] == 'Admin':
        return redirect('/admin/viewQuestion')
    else:
        return redirect('/')
@app.route('/user/Test')
def userTest():
    if session['Role'] == 'User':
        return render_template('Test.html')
    elif session['Role'] == 'Admin':
        return redirect('/admin/loadDashboard')
    else:
        return redirect('/')

@app.route('/user/feedback')
def userFeedback():
    if session['Role'] == 'User':
        return render_template('Ufeedback.html')
    elif session['Role'] == 'Admin':
        return redirect('/admin/loadDashboard')
    else:
        return redirect('/')


@app.route('/user/feedbackData', methods=['POST'])
def userFeedbackData():
    feedback = request.form['feedback']
    rating = request.form['rating']
    username = session['Username']
    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()
    cur.execute(f"insert into feedbackmaster(username, feedback, rating) values('{username}', '{feedback}', '{rating}')")
    connection.commit()
    cur.close()
    connection.close()
    return redirect('/user/feedback')


@app.route('/admin/viewFeedback')
def adminViewFeedback():
    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()
    cur.execute(f"select * from feedbackmaster")
    data = cur.fetchall()
    cur.close()
    connection.close()
    return render_template('viewFeedback.html', data=data)


@app.route('/forgotPassword')
def forgotPassword():
    return render_template('forgotPassword.html')


# @app.route('/forgotPasswordData', methods=['POST'])
# def forgotPasswordData():
#     email = request.form['email']
#     connection = pymysql.connect(
#         host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
#         user='root',
#         password='rootroot',
#         db='smartpi'
#     )
#     cur = connection.cursor()
#     cur.execute(f"select User_Name, User_Pwd, Email from registermaster")
#     data = cur.fetchone()
#     return render_template('forgotPassword.html', message1=f"Username: {data[0]}  Password: {data[1]}")


@app.route('/logout')
def logout():
    session.pop('Username', None)
    session.pop('Role', None)
    print(session)
    return redirect('/')


@app.route('/forgotPasswordData', methods=['GET', 'POST'])
def forgotPasswordData():
    email = request.form['email']
    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()
    cur.execute(f"select User_Name, User_Pwd, Email from registermaster")
    data = cur.fetchall()

    def send_email(data):
        sending_address = "infinitypack1@gmail.com"
        receiving_address = request.form['email']
        msg = MIMEMultipart()
        msg['From'] = sending_address
        msg['To'] = receiving_address
        msg['Subject'] = "SmartPI - Forgot password"
        msg['Body'] = f"USERNAME: {data[0]} PASSWORD: {data[1]}"
        msg.attach(MIMEText(msg['Body'], 'plain'))
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(sending_address, "badass@123$$$")
        text = msg.as_string()
        server.sendmail(sending_address, receiving_address, text)
        server.quit()
        print('Done')
        return render_template('forgotPassword.html', message1='Email sent!')

    for item in data:
        if item[2] == email:
            send_email(item)
            break
    else:
        return render_template('forgotPassword.html', message='Email not found!')


@app.route('/test')
def test():
    connection = pymysql.connect(
        host='smartpi.crua8l4ztgso.us-east-1.rds.amazonaws.com',
        user='root',
        password='rootroot',
        db='smartpi'
    )
    cur = connection.cursor()
    cur.execute("select * from questionsmaster where TopicName='General';")
    print(cur.fetchone())
    return redirect('/')


if __name__ == '__main__':
    app.run()
    session.pop('Username', None)
    session.pop('Role', None)

# SMTP Email
